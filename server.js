const express = require('express');
var serveIndex = require('serve-index');
var fs = require('fs');

const app = express();


app.use(express.static("public"));
//Wenn serveIndex verwendet wird schlägt POST fehl
app.use("/explore/",serveIndex('public'));


app.get("/chatanalyse/save", function (req, res) {
    console.log("angekommen");
    res.send("Funktionioert");
});

app.get("/survivalAssistant/", function (req, res) {
    console.log("Anfrage erhalten");
    res.send("Anfrage erhalten");
});

app.post("/survivalAssistant/", function (req, res) {
    console.log("Post - Anfrage erhalten");
    //res.send("Anfrage erhalten");
});

app.use(express.json({limit: "1mb"}));
app.post("/chatanalyse/save", function (req, res) {
    const json = JSON.stringify((req.body));
    console.log("POST empfangen");

    var pathToImages = "./public/chatanalyse/shared/";
    if (!fs.existsSync(pathToImages)){
        console.log("Ornder existiert nicht");
        fs.mkdirSync(pathToImages);
        console.log("Ornder wurde erstellt");
    }
    let hash = json.hashCode();
    fs.writeFileSync(pathToImages + hash + ".json", json, 'utf8');
    console.log("Hash code: " + json.hashCode());
    res.send(hash.toString());

});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
});

String.prototype.hashCode = function(){
    var hash = 0;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}