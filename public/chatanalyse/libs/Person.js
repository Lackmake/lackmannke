class Person{
    constructor(name){
        this.name = name;
        this.color = "";

        this.messageCount = 0;
        this.wordCount =  0;
        this.mediaCount = 0;
        this.emojiCount = 0;

        this.words = {};
        this.emojis = {};
    }


    //Todo: warum kopier ich das erst in ne neue liste? mal ausprobieren ob das sort auch auf der ursprünglichen liste funktioniert
    //--> die originalen Listen haben die Wörter als index, dann kann ich nicht mit schleife durchiterieren
    sortResults(){
        let sortedEmojis = [];

        for (let emoji in this.emojis) {
            sortedEmojis.push([emoji, this.emojis[emoji]])
        }

        sortedEmojis.sort(function (a, b) {
            return b[1] - a[1];
        });
        this.emojis = sortedEmojis;

        let sortedWords = [];

        for (let word in this.words) {
            sortedWords.push([word, this.words[word]])
        }

        sortedWords.sort(function (a, b) {
            return b[1] - a[1];
        });
        this.words = sortedWords;
    }

}