let totalMessageCount = 0;
let totalWordCount = 0;
let totalMediaCount = 0;
let totalEmojiCount = 0;

let messagePerDate = [];
let dayswithmessages = [];
let messagesPerWeekday = [];
let messagesPerHour = [];

let personResults = {};

let dateFirst;
let dateLast;

let chattext;// = document.getElementById("chat").value.split("\n");

let share = false;


//TODO Ergebnisse teilbar machen


//TODO clear results bevor neu ausführen, sonst wird das nur unten angehängt, außerdem diagramme erst angzeigen wenn gefüllt, sonst scrollbalken obwohl nichts zu sehen
function readFile() {
    const file = document.getElementById("chat").files[0];

    const fr = new FileReader();
    fr.onload = function (e) {
        //TODO: Sonderzeichen sind bisher nur üöäÜÖÄß
        const regex = /\n(?=[0-9][0-9].[0-9][0-9].[0-9][0-9], [0-9][0-9]:[0-9][0-9] . [a-zA-Z\u00c4\u00e4\u00d6\u00f6\u00dc\u00fc\u00df,\- ]*: )/;
        chattext = e.target.result.split(regex);
        analyse();
    };
    fr.readAsText(file);
}

function analyse() {
    let zahl = 0; //Für Debugzwecke

    detectTimespan();

    for (let i = 0; i < chattext.length; ++i) {
        const line = chattext[i];
        const message = line.substring(getPosition(line, ":", 2) + 2, line.length);
        let name = line.substring(18, getPosition(line, ":", 2));

        //Wenn der Name Länger ist handelt es sich vermutlich um eine Meldung wie zB eine Änderung des Gruppenbildes, daher ignorieren
        if (name.length > 36) {
            continue;
        }

        if (!personResults[name]) {
            personResults[name] = new Person(name);
        }

        //Bei Medien können nicht die Wörter analysiert werden, also continue
        if (isMedia(message, name)) {
            continue;
        }

        totalMessageCount++;
        personResults[name].messageCount++;

        //Splitten der Nachricht in die Wörter
        const words = message.replace(/[.?!,;]/g, '').split(/\s/);
        analyseWords(words, name);

        analyseEmojis(message, name);

        const day = line.substring(0, 2);
        const month = line.substring(3, 5);
        const year = line.substring(6, 8);
        const messageDate = new Date(month + "/" + day + "/" + year);
        const oneDay = 24 * 60 * 60 * 1000;
        //Differenz von der ersten Nachricht des Logs bis zur Aktuellen, als Index für das Array benötigt
        const daysSinceFirstMessage = Math.round(Math.abs((messageDate.getTime() - dateFirst.getTime()) / (oneDay)));
        messagePerDate[daysSinceFirstMessage]++;

        const weekday = messageDate.getDay();//0-6, 0=Sunday
        increment(messagesPerWeekday, weekday);

        let hour = Number(line.substring(10, 12));
        let minute = Number(line.substring(13, 15));

        if (minute > 30) {
            if (hour < 24) {
                hour++;
            } else if (hour === 24) {
                hour = 0;
            }
        }
        increment(messagesPerHour, hour);
    }
    console.log("Analyse beendet...................................");

    showResults();
}

function analyseWords(words, name) {
    words.forEach(function (word) {
        word = word.toLowerCase();
        totalWordCount++;
        personResults[name].wordCount++;
        if (word !== "") {
            increment(personResults[name].words, word);
        }
    });
}

function analyseEmojis(message, name) {
    for (let character of message) {
        if (character.length === 1) continue;
        increment(personResults[name].emojis, character);
        totalEmojiCount++;
        personResults[name].emojiCount++;
    }
}

function isMedia(message, name) {
    if (message === "<Medien weggelassen>") {
        totalMediaCount++;
        personResults[name].mediaCount++;
        return true;
    } else {
        return false;
    }
}

function detectTimespan() {
    const firstDay = chattext[0].substring(0, 2);
    const firstMonth = chattext[0].substring(3, 5);
    const firstYear = chattext[0].substring(6, 8);
    dateFirst = new Date(firstMonth + "/" + firstDay + "/" + firstYear);

    const lastDay = chattext[chattext.length - 1].substring(0, 2);
    const lastMonth = chattext[chattext.length - 1].substring(3, 5);
    const lastYear = chattext[chattext.length - 1].substring(6, 8);
    dateLast = new Date(lastMonth + "/" + lastDay + "/" + lastYear);

    const varidate = new Date(dateFirst.getTime());
    let indo = 0;
    while (varidate <= dateLast) {
        const datestring = varidate.getDate() + "." + (Number(varidate.getMonth()) + 1) + "." + varidate.getFullYear();
        dayswithmessages.push(datestring);
        messagePerDate[indo] = 0;
        indo++;
        varidate.setDate(varidate.getDate() + 1);
    }
}


//TODO in extra Datei
function showResults() {
    document.getElementById("messageCount").innerHTML = totalMessageCount;
    document.getElementById("wordCount").innerHTML = totalWordCount;
    document.getElementById("emojiCount").innerHTML = totalEmojiCount;
    document.getElementById("mediaCount").innerHTML = totalMediaCount;

    let participants = [];
    let participation = [];

    for (let person in personResults) {
        person = personResults[person];
        if(!share){
            person.sortResults();
        }

        participants.push([person.name,person.messageCount]);

        createResultLists(person.name, person.words, "topwords");
        createResultLists(person.name, person.emojis, "topemojis");
        createPersonalResults(person);
    }

    participants.sort(function (a, b) {
        return b[1]-a[1];
    });
    for(let i = 0; i < participants.length;i++){
        participation[i] = participants[i][1];
        participants[i] = participants[i][0];
    }
    console.log(participants);


    //TODO durchschnitt nachrichten pro tag bzw pro aktivem tag
    //wörter pro nachricht


    let ctx = document.getElementById("datesChart").getContext('2d');
    let datesChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dayswithmessages,
            datasets: [{
                label: '# of messages',
                data: messagePerDate,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    let ctx1 = document.getElementById("timeOfDayChart").getContext('2d');
    let timeOfDayChart = new Chart(ctx1, {
        type: 'bar',
        data: {
            labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            datasets: [{
                label: '# of messages',
                data: messagesPerHour,
                borderWidth: 1
            }]
        },
        // options: {
        //     scales: {
        //         yAxes: [{
        //             ticks: {
        //                 beginAtZero: true
        //             }
        //         }]
        //     }
        // }
    });

    let ctx2 = document.getElementById("weekdayChart").getContext('2d');
    let weekDaychart = new Chart(ctx2, {
        type: 'bar',
        data: {
            //TODO Sonntag nach hinten verschieben
            labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            datasets: [{
                label: '# of messages',
                data: messagesPerWeekday,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    // And for a doughnut chart
    let ctx3 = document.getElementById("inclusionChart").getContext('2d');
    let myDoughnutChart = new Chart(ctx3, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: participation
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: participants
        }
    });


    let list = personResults["Kevin Lackmann"].words;
    let shortlist = [];
    for (var i = 0; i < 200; i++) {
        shortlist.push(list[i]);
    }
    WordCloud(document.getElementById('wordcloud'),
        {
            list: shortlist,
            shape: "circle",
            weightFactor: 0.15
        });
}

function createResultLists(name, topItemList, container) {
    let personDiv = document.createElement("div");
    let nameTitle = document.createElement("h5");
    nameTitle.innerHTML = name;
    personDiv.appendChild(nameTitle);
    let list = document.createElement("ol");
    for (i = 0; i < 20; i++) {
        let item = topItemList[i];
        let listItem = document.createElement("li");
        if (item) {
            listItem.innerHTML = item[0] + " " + item[1];
        } else {
            listItem.innerHTML = "-";
        }
        list.appendChild(listItem);
    }
    personDiv.appendChild(list);
    document.getElementById(container).appendChild(personDiv);
}

function createPersonalResults(person) {
    const elements = "<div><h5>" + person.name + "</h5>" +
        "<p>Nachrichten: " + person.messageCount + "</p>" +
        "<p>Worte: " + person.wordCount + "</p>" +
        "<p>Medien: " + person.mediaCount + "</p>" +
        "<p>Emojis: " + person.emojiCount + "</p></div>";

    document.getElementById("personalResults").insertAdjacentHTML("beforeend", elements);

}


function getPosition(string, subString, index) {
    return string.split(subString, index).join(subString).length;
}

function increment(array, index) {
    if (array[index]) {
        array[index]++;
    } else {
        array[index] = 1;
    }
}

function saveResults() {
    personResults["TotalStatistics"] = {};
    personResults["TotalStatistics"].messagePerDate = messagePerDate;
    personResults["TotalStatistics"].dayswithmessages = dayswithmessages;
    personResults["TotalStatistics"].messagesPerWeekday = messagesPerWeekday;
    personResults["TotalStatistics"].messagesPerHour = messagesPerHour;

    personResults["TotalStatistics"].totalMessageCount = totalMessageCount;
    personResults["TotalStatistics"].totalWordCount = totalWordCount;
    personResults["TotalStatistics"].totalMediaCount = totalMediaCount;
    personResults["TotalStatistics"].totalEmojiCount = totalEmojiCount;

    var request = new XMLHttpRequest();
    var url = "./save/";
    request.open("POST", url, true);
    request.setRequestHeader("Content-type", "application/json");
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            // var json = JSON.parse(request.responseText);
            console.log("Erfolgreich versendet");
            console.log(request.responseText);
            document.getElementById("shareUrl").value = window.location.href + "?id=" + request.responseText;
        }else{
            console.log(request.status);
        }
    };
    request.send(JSON.stringify(personResults));
}